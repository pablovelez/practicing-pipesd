import { configureStore } from '@reduxjs/toolkit'
import { userReducer } from './reducers'
import todoReducer from './reducers/todos'

export const store = configureStore({
  reducer: {
      todo: todoReducer,
      user: userReducer
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export const todoSelector = (state:RootState) => state.todo