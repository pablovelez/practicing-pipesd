import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
export type Todo ={
    text:string
    id:number
    isDone: boolean
}

export interface TodosState{
    todos: Todo[]
}
const initialState:TodosState = {
    todos: [{id:1,text:'pinga', isDone:false}]
}

export const todosSlice = createSlice({
    name:'todos',
    initialState,
    reducers: {
        addTodo:(state,payload: PayloadAction<Todo>)=>{
           state.todos.push(payload.payload)
        },
        removeTodo: (state, payload:PayloadAction<number>)=>{
           state.todos = state.todos.filter(todo => todo.id !== payload.payload)
        }

    }
})

export const {addTodo, removeTodo} = todosSlice.actions
export default todosSlice.reducer