import  { FC }  from 'react'
import styled from 'styled-components'

interface PropsWithChildren {
    children: React.ReactNode
}

const LayoutContainer = styled.div`
    background-color: #002e63;
    padding:2rem;
    height:100vh
`

export const Layout:FC<PropsWithChildren> = ({children}) => {
    return (
        <LayoutContainer>
            {children}
        </LayoutContainer>
    )
}
