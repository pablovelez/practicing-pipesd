import { ApiUser, User } from "../../../models";

export const MortyAdapter = (user:ApiUser):User => {
    return {
        id: user.id,
        name: user.name
    }
}