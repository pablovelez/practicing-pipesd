import { Title } from "../../styled-components/title"
import { Layout } from "../../components/layout"
import { useDispatch } from "react-redux"
import { useEffect } from "react"
import { createUser } from "../../redux/reducers/user.slice"
import { useCallback } from "react"
import { fetchMorty, rickAndMortyUrl } from "./services"
import { useState } from "react"
import { User } from "../../models"

 const Home = () => {

    const  dispatch = useDispatch()
    const [morty, setMorty] = useState<User>()
    
    const dispatchAction = useCallback(() => dispatch(createUser(morty)),[dispatch, morty])

    const fetchCharacter = async() => {
        const data = await fetchMorty(rickAndMortyUrl)
        setMorty(data)
    }

    useEffect(() => {
        fetchCharacter()
    }, [])

    return (
        <Layout>
           <Title>Tuki</Title>
           <button
           onClick={dispatchAction}
           >Mnadar usuario</button>
        </Layout>
    )
}

export default Home
