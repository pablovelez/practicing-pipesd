import { useEffect, useState } from "react"
import { AxiosResponse } from 'axios';

export interface AxiosCall<T> {
  call: Promise<AxiosResponse<T>>;
  controller?: AbortController;
}

export const useFetchAndLoad = async () => {
   const [isLoading, setisLoading] = useState(false)
    let controller: AbortController
   const callEndpoint = async (axiosCall: AxiosCall<any>) => {
       if(axiosCall.controller) controller = axiosCall.controller
       let result= {} as AxiosResponse<any>
    setisLoading(true)
    try {
        result = await axiosCall.call
    } catch (error:any) {
        throw new Error(error.message)
    }finally{
        setisLoading(false)
    }
    return result
   }

   const cancelController = () => {
    setisLoading(false)
    controller && controller.abort()
   }

   useEffect(() => {
       return () =>{
        cancelController()
       }
   }, [])

   return {callEndpoint, loader: isLoading}
}
