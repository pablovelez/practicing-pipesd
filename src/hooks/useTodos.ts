import { useDispatch, useSelector } from "react-redux"
import { removeTodo, Todo } from "../redux/reducers/todos"
import { todoSelector } from "../redux/store"
import { addTodo } from '../redux/reducers/todos';


export const useTodos = () => {
  const {todos} = useSelector(todoSelector)
  const dispatch = useDispatch()
  const add = (todo:Todo) => {
    dispatch(addTodo(todo))
  }
  const remove = (id:number) => {
    dispatch(removeTodo(id))
  }
  return {
      addTodo:add,removeTodo:remove, todos
  }
}