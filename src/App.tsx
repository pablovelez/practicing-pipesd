import { lazy, Suspense } from 'react'
const Home = lazy(()=> import('./pages/home/home'))

const App = () => {

    return (
        <Suspense fallback={<span>Loading...</span>}>
            <Home />
        </Suspense>
    )
}

export default App
