import './App.css';
import { useState } from 'react';
import { useTodos } from './hooks/useTodos';

function App() {

  const [text, setText] = useState('')
  const {todos, addTodo, removeTodo} = useTodos()

  const handleAdd = (e:React.SyntheticEvent) => {
    e.preventDefault()
    const newTodo = {id: Date.now(), text, isDone: false}
    addTodo(newTodo)
    setText('')
  }
  return (
    <div className="App">
      <form onSubmit={handleAdd}>
        <input
        value={text}
        onChange={(e:React.FormEvent<HTMLInputElement>) => setText(e.currentTarget.value)}
        />
        <button type='submit' >Add</button>
      </form>
   {
     todos.map(todo =>(
       <div
       key={todo.id}>
         {todo.text}
         <button
         onClick={()=> removeTodo(todo.id)}
         >
           Borrar
         </button>
       </div>
     ))
   }
    </div>
  );
}

export default App;
