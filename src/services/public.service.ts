import { loadAbort } from "../utilities/load-abort-axios-utility"
import axios from 'axios'
import { User } from "../models"
import { rickAndMortyUrl } from "../pages/home"

export const login = () => {
    const controller =  loadAbort()
    return {
        call: axios.get<User>(rickAndMortyUrl,{
            signal: controller.signal
        } ),
        controller
    }
}